Teil 1:

Im ersten Schritt haben wir das C-Programm (householder.c) heruntergeladen und mithilfe MPI parallelisiert, kompiliert und am Ende

Ausgeführt.

Um das Programm zu parallelisieren, haben wir Folgendes hinzufügt:

 int rank;

 int size ;

 char name[80];

 int length;

 MPI\_Init(&argc, &argv)

 MPI\_Comm\_rank(MPI\_COMM\_WORLD, &rank);

 MPI\_Comm\_size(MPI\_COMM\_WORLD, &size);

 MPI\_Get\_processor\_name(name, &Length);

Das hat uns 0.003 Sekunden gedauert. So bekommen wir eine Entschleunigung von 0.002S

Dann haben wir die drei Implementierungen vergleicht, indem wir 

1-Die Kosten der Programme 

2-Speedup von den zwei parallelen Versionen in Bezug auf das seqzuenzielle

3-Effizienz von den zwei parallelen Versionen 

berücksichtigt

Kosten der Programme 

Cp(n) = Tp(n) · p

Sequenzielle

Cp(n) = 0.002 ∗ 6 = 0.012

OMP

Cp(n) = 0.006 ∗ 6 = 0.036

MPI

Cp(n) = 0.003\*1 = 0.003

Speedup von den zwei parallelen Versionen in Bezug auf das seqzuenzielle

Sp(n) = T∗(n)/Tp(n)

OMP

Sp(n) = 0.002/0.006 =0.333s

Mpi 

Sp(n) = 0.002/0.003=0.666

Effizienz von den zwei parallelen Versionen 

Ep(n) = T∗(n)/Cp(n)

OMP

Ep(n) = 0.002/0.036 = 0.055555556

MPI

Ep(n) = 0.002/0.003= 0.003



Teil 2:

Am Anfang haben wir das C-Programm, das zu Multiplikation von Matrizen dient. Mithilfe von MPI parallelisiert.

Um das Programm zu parallelisieren, haben wir Folgendes hinzufügt:

 int rank;

 int size ;

 char name[80];

 int length;

 MPI\_Init(&argc, &argv)

 MPI\_Comm\_rank(MPI\_COMM\_WORLD, &rank);

 MPI\_Comm\_size(MPI\_COMM\_WORLD, &size);

 MPI\_Get\_processor\_name(name, &Length);

Dann haben wir die drei Implementierungen an C-Programm, das zu Multiplikation von Matrizen dient, vergleicht, indem wir 

1-Die Kosten der Programme 

2-Speedup von den zwei parallelen Versionen in Bezug auf das seqzuenzielle

3-Effizienz von den zwei parallelen Versionen 

berücksichtigt

Kosten der Programme 

Cp(n) = Tp(n) · p

Sequenzielle

Cp(n) =0.002 \* 6 = 0.012

OMP

Cp(n) = 0.002 \* 6 = 0.012

MPI

Cp(n) = (0.003)\*1 = 0.003

Speedup von den zwei parallelen Versionen in Bezug auf das seqzuenzielle

Sp(n) = T∗(n)/Tp(n)

OMP

Sp(n) = 0.002/0.002 =1

Mpi 

Sp(n) = 0.002/(0.003) =0.66

Effizienz von den zwei parallelen Versionen 

Ep(n) = T∗(n)/Cp(n)

OMP

Ep(n) = 0.002/0.012 = 0.1666667

MPI

Ep(n) = 0.002/(0.003) =0.66
